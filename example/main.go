package main

import (
	//"context"
	"fmt"
	"log"

	vector_search "gitlab.com/barkingdog-ai/vector-search"
	"gitlab.com/barkingdog-ai/vector-search/pinecone/utils"
)

func main() {
	apiKey := utils.MustGetEnv("PINECONE_APIKEY")

	index := utils.MustGetEnv("PINECONE_INDEX")
	if index == "" {
		log.Fatalln("Missing INDEX")
	}
	client, err := vector_search.NewClient(apiKey, index)

	fmt.Println(client)
	if err != nil {
		log.Fatalln(err)
	}

	// ctx := context.Background()

}
