package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/utils"
)

func main() {
	apiKey := utils.MustGetEnv("PINECONE_APIKEY")

	url := "https://controller.us-west1-gcp.pinecone.io/collections"

	payload := strings.NewReader("{\"name\":\"amaze-knowledge-base\",\"source\":\"amaze\"}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("accept", "text/plain")
	req.Header.Add("content-type", "application/json")
	req.Header.Add("Api-Key", apiKey)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))
}
