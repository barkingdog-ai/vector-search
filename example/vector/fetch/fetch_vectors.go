package main

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/utils"
)

func main() {
	baseURL := utils.MustGetEnv("PINECONE_BASE_URL")
	namespace := utils.MustGetEnv("PINECONE_NAMESPACE")
	apiKey := utils.MustGetEnv("PINECONE_APIKEY")
	url := baseURL + "/vectors/fetch?namespace=" + namespace
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("accept", "application/json")
	req.Header.Add("Api-Key", apiKey)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))
}
