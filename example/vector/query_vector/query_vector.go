package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/utils"
)

type Payload struct {
	IncludeValues   string                 `json:"includeValues"`
	IncludeMetadata string                 `json:"includeMetadata"`
	TopK            int                    `json:"topK"`
	ID              string                 `json:"id"`
	Namespace       string                 `json:"namespace"`
	Filter          map[string]interface{} `json:"filter"`
	Vector          []float64              `json:"vector"`
}

func main() {
	baseURL := utils.MustGetEnv("PINECONE_BASEURL")
	queryURL := baseURL + "/query"

	apiKey := utils.MustGetEnv("PINECONE_APIKEY")
	// openAIKey := utils.MustGetEnv("OPENAI_APIKEY")
	// azureApiKey := utils.MustGetEnv("AZURE_OPENAI_APIKEY")
	// azureDeployment := utils.MustGetEnv("AZURE_DEPLOYMENT")

	filter := map[string]interface{}{
		// "knowledge_id": map[string]interface{}{
		// 	"$in": []string{"130eccf4-19c6-4b01-8f04-9c5295803ee6"},
		// },
		"knowledge_id": map[string]interface{}{
			"$in": []string{"c6dfad34-4845-460c-8f04-94bd0adcc176"},
		},

		// "price": "105",
		// "price": map[string]interface{}{
		// 	"$gte": 100,
		// },
		// "price": map[string]interface{}{
		// 	"$lt": 100,
		// },
	}
	// text := "What about Ramen? Is it Japanese food?"

	// openaiClient := openai.NewClient(openAIKey, azureApiKey, azureDeployment)
	// ctx := context.Background()
	// vector, _, err := openaiClient.GetDocEmbedding(ctx, text)

	payloadData := Payload{
		// Vector:          vector,
		IncludeValues:   "false",
		IncludeMetadata: "true",
		TopK:            10,
		ID:              "64d7a5ca-50a6-4e7f-a0dd-37212d9228c8",
		Namespace:       "dev-test-0706", //"ai-amaze-knowledges-dev",
		Filter:          filter,
	}
	payloadBytes, err := json.Marshal(payloadData)
	if err != nil {
		fmt.Println("JSON marshaling error:", err)
		return
	}

	payloadString := string(payloadBytes)
	fmt.Println("payloadString >>>> ", payloadString)
	payloadReader := strings.NewReader(payloadString)

	req, _ := http.NewRequest("POST", queryURL, payloadReader)

	req.Header.Add("accept", "application/json")
	req.Header.Add("content-type", "application/json")
	req.Header.Add("Api-Key", apiKey)

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	// 將結果寫入本地JSON檔案
	err = ioutil.WriteFile("result.json", body, 0o644)
	if err != nil {
		fmt.Println("寫入檔案時發生錯誤:", err)
		return
	}

	fmt.Println("結果已成功寫入 result.json 檔案")
}
