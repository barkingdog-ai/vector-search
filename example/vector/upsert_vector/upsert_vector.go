package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/utils"
)

func main() {
	baseURL := utils.MustGetEnv("PINECONE_BASE_URL")
	url := baseURL + "/vectors/upsert"
	apiKey := utils.MustGetEnv("PINECONE_APIKEY")
	payload := strings.NewReader("{\"vectors\":[{\"values\":[-0.0016800201, -0.013895879 ,0.0016791438],\"id\":\"6920fc10-e11a-42f9-8b1c-a658470217ce\"}],\"namespace\":\"amaze\"}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("accept", "application/json")
	req.Header.Add("content-type", "application/json")
	req.Header.Add("Api-Key", apiKey)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))
}
