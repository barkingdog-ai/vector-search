package api

import (
	"context"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/model"
)

// Pod Types
const (
	S1_X1 = "s1.x1"
	S1_X2 = "s1.x2"
	S1_X4 = "s1.x4"
	S1_X8 = "s1.x8"

	P1_X1 = "p1.x1"
	P1_X2 = "p1.x2"
	P1_X4 = "p1.x4"
	P1_X8 = "p1.x8"

	P2_X1 = "p2.x1"
	P2_X2 = "p2.x2"
	P2_X4 = "p2.x4"
	P2_X8 = "p2.x8"
)

// Metrics
const (
	Cosine     = "cosine"
	DotProduct = "dotproduct"
	Euclidean  = "euclidean"
)

const (
	defaultMetric    = Cosine
	defaultPods      = 1
	defaultReplicas  = 1
	defaultPodType   = P1_X1
	defaultDimension = 1536
)

type IndexInterface interface {
	CreateIndexRequest(ctx context.Context, name string, options ...model.IndexOption) (*model.CreateIndexResponse, error)
	CreateIndex(ctx context.Context, request model.Index) (*model.CreateIndexResponse, error)

	DescribeIndexRequest(ctx context.Context, area string, index_name string) (*model.DescribeIndexResponse, error)
	DescribeIndex(ctx context.Context, request model.Index) (*model.DescribeIndexResponse, error)
	/*
		ConfigureIndexRequest(ctx context.Context, name string) (*model.DeleteIndexResponse, error)
		ConfigureIndex(ctx context.Context, request model.Index) (*model.DeleteIndexResponse, error)
		DeleteIndexRequest(ctx context.Context, name string) (*model.DeleteIndexResponse, error)
		DeleteIndex(ctx context.Context, request model.Index) (*model.DeleteIndexResponse, error)
	*/
}

func (c *VectorSearchClient) CreateIndexRequest(ctx context.Context, name string, options ...model.IndexOption) (*model.CreateIndexResponse, error) {
	request := model.Index{
		Name:      name,
		Dimension: defaultDimension,
		Metric:    defaultMetric,
		Pods:      defaultPods,
		Replicas:  defaultReplicas,
		PodType:   defaultPodType,
	}
	for _, o := range options {
		o(&request)
	}
	response, err := c.CreateIndex(ctx, request)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) CreateIndex(ctx context.Context, request model.Index) (*model.CreateIndexResponse, error) {
	req, err := c.newRequest(ctx, "POST", "/databases", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.CreateIndexResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}

func (c *VectorSearchClient) DescribeIndexRequest(ctx context.Context, area string, index_name string) (*model.DescribeIndexResponse, error) {
	request := model.Index{
		Name: index_name,
	}
	response, err := c.DescribeIndex(ctx, request)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) DescribeIndex(ctx context.Context, request model.Index) (*model.DescribeIndexResponse, error) {
	req, err := c.newRequest(ctx, "GET", "/databases/"+request.Name, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.DescribeIndexResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}

/*
func (c *VectorSearchClient) ConfigureIndexRequest(ctx context.Context, name string) (*model.DeleteIndexResponse, error) {

	request := model.Index{
		Name: name,
	}
	response, err := c.ConfigureIndex(ctx, request)
	if err != nil {
		return nil, err
	}
	return response, nil
}
func (c *VectorSearchClient) ConfigureIndex(ctx context.Context, request model.Index) (*model.DeleteIndexResponse, error) {
	req, err := c.newRequest(ctx, "PATCH", "/databases", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.DeleteIndexResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}

func (c *VectorSearchClient) DeleteIndexRequest(ctx context.Context, name string) (*model.DeleteIndexResponse, error) {

	request := model.Index{
		Name: name,
	}
	response, err := c.DeleteIndex(ctx, request)
	if err != nil {
		return nil, err
	}
	return response, nil
}
func (c *VectorSearchClient) DeleteIndex(ctx context.Context, request model.Index) (*model.DeleteIndexResponse, error) {
	req, err := c.newRequest(ctx, "DELETE", "/databases", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.DeleteIndexResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}

*/
