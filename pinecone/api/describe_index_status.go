package api

import (
	"context"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/model"
)

type DescribeIndexStatsInterface interface {
	DescribeIndexStats(ctx context.Context, request model.DescribeIndexStatsRequest) (*model.DescribeIndexStatsResponse, error)
}

func (c *VectorSearchClient) DescribeIndexStats(ctx context.Context, request model.DescribeIndexStatsRequest) (*model.DescribeIndexStatsResponse, error) {
	req, err := c.newRequest(ctx, "POST", "/describe_index_stats", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.DescribeIndexStatsResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}
