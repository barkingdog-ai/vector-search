package api

import (
	"net/http"
)

type VectorSearchClient struct {
	BaseURL    string
	IndexURL   string
	Area       string
	ApiKey     string
	IndexName  string
	HttpClient *http.Client
}
