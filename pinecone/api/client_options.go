package api

import (
	"net/http"
)

// ClientOption are options that can be passed when creating a new client
type ClientOption func(*VectorSearchClient) error

// WithBaseURL is a client option that allows you to override the default base url of the client.
// The default base url is "https://api.VectorSearch.com/v1"
func WithBaseURL(baseURL string) ClientOption {
	return func(c *VectorSearchClient) error {
		c.BaseURL = baseURL
		return nil
	}
}

func WithIndexName(indexName string) ClientOption {
	return func(c *VectorSearchClient) error {
		c.IndexName = indexName
		return nil
	}
}

// WithHTTPClient allows you to override the internal http.Client used
func WithHTTPClient(httpClient *http.Client) ClientOption {
	return func(c *VectorSearchClient) error {
		c.HttpClient = httpClient
		return nil
	}
}
