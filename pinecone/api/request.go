package api

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/model"
	"gitlab.com/barkingdog-ai/vector-search/pinecone/utils"
)

func (c *VectorSearchClient) doRequest(method, path string, payload interface{}) (*http.Response, error) {
	bodyReader, err := jsonBodyReader(payload)
	if err != nil {
		return nil, err
	}
	var request *http.Request
	url := c.BaseURL + path
	if method == "GET" {
		request, _ = http.NewRequest(method, url, nil)
		q := request.URL.Query()
		val := reflect.ValueOf(payload)
		// fmt.Println("========new request val: ", val)
		if val.Kind() == reflect.Struct {
			for i := 0; i < val.NumField(); i++ {
				field := val.Type().Field(i)
				key := field.Name
				value := val.Field(i)
				if value.Kind() == reflect.Int {
					q.Add(utils.ToSnakeCase(key), strconv.Itoa(int(value.Int())))
				} else if value.Kind() == reflect.String {
					q.Add(utils.ToSnakeCase(key), value.String())
				} else if value.Kind() == reflect.Slice {
					for i := 0; i < value.Len(); i++ {
						q.Add(utils.ToSnakeCase(key), value.Index(i).String())
					}
				}
			}
		}
		request.URL.RawQuery = q.Encode()
	} else {
		request, _ = http.NewRequest(method, url, bodyReader)
	}
	if err != nil {
		return nil, err
	}
	request.Header.Add("accept", "application/json")
	request.Header.Set("Content-type", "application/json")
	request.Header.Add("Api-Key", c.ApiKey)
	resp, err := c.HttpClient.Do(request)
	if err != nil {
		return nil, err
	}
	if err := checkForSuccess(resp); err != nil {
		return nil, err
	}
	return resp, nil
}

func (c *VectorSearchClient) newRequest(ctx context.Context, method, path string, payload interface{}) (*http.Request, error) {
	bodyReader, err := jsonBodyReader(payload)
	if err != nil {
		return nil, err
	}
	var request *http.Request
	url := c.BaseURL + path
	if method == "GET" {
		request, _ = http.NewRequest(method, url, nil)
		q := request.URL.Query()
		val := reflect.ValueOf(payload)
		// fmt.Println("========new request val: ", val)
		if val.Kind() == reflect.Struct {
			for i := 0; i < val.NumField(); i++ {
				field := val.Type().Field(i)
				key := field.Name
				value := val.Field(i)
				// fmt.Println("key: ", key, "value: ", value, "value kind: ", value.Kind().String())
				if value.Kind() == reflect.Int {
					q.Add(utils.ToSnakeCase(key), strconv.Itoa(int(value.Int())))
				} else if value.Kind() == reflect.String {
					q.Add(utils.ToSnakeCase(key), value.String())
				} else if value.Kind() == reflect.Slice {
					for i := 0; i < value.Len(); i++ {
						q.Add(utils.ToSnakeCase(key), value.Index(i).String())
					}
				}
			}
		}
		request.URL.RawQuery = q.Encode()
	} else {
		request, err = http.NewRequestWithContext(ctx, method, url, bodyReader)
	}
	if err != nil {
		return nil, err
	}
	request.Header.Add("accept", "application/json")
	request.Header.Set("Content-type", "application/json")
	request.Header.Add("Api-Key", c.ApiKey)

	return request, nil
}

func (c *VectorSearchClient) performRequest(req *http.Request) (*http.Response, error) {
	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}
	if err := checkForSuccess(resp); err != nil {
		return nil, err
	}
	return resp, nil
}

// returns an error if this response includes an error.
func checkForSuccess(resp *http.Response) error {
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		return nil
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to read from body: %w", err)
	}
	var result model.APIErrorResponse
	if err := json.Unmarshal(data, &result); err != nil {
		// if we can't decode the json error then create an unexpected error
		APIError := model.APIError{
			StatusCode: resp.StatusCode,
			Type:       "Unexpected",
			Message:    string(data),
		}
		return APIError
	}
	result.Error.StatusCode = resp.StatusCode
	return result.Error
}

func jsonBodyReader(body interface{}) (io.Reader, error) {
	if body == nil {
		return bytes.NewBuffer(nil), nil
	}
	raw, err := json.Marshal(body)
	if err != nil {
		return nil, fmt.Errorf("failed encoding json: %w", err)
	}
	return bytes.NewBuffer(raw), nil
}

func getResponseObject(rsp *http.Response, v interface{}) error {
	defer rsp.Body.Close()
	if err := json.NewDecoder(rsp.Body).Decode(v); err != nil {
		return fmt.Errorf("invalid json response: %w", err)
	}
	return nil
}
