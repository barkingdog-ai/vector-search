package api

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/barkingdog-ai/vector-search/pinecone/model"
)

type VectorInterface interface {
	// Create vector
	UpsertRequest(ctx context.Context, namespace string, id string, vector []float64, metadata interface{}) (*model.UpsertResponse, error)
	Upsert(ctx context.Context, request model.UpsertRequest) (*model.UpsertResponse, error)
	SendUpsertRequest(namespace string, id string, vector []float64, metadata interface{}) (*model.UpsertResponse, error)

	QueryProductRequestWithFilter(ctx context.Context, namespace string, knowledge_list []string, top_k int, vector []float64, price int, category []string, orderBy string) (*model.QueryResponse, error)
	QueryRequestWithFilter(ctx context.Context, namespace string, knowledge_list []string, top_k int, vector []float64) (*model.QueryResponse, error)
	QueryRequest(ctx context.Context, namespace string, top_k int, vector []float64) (*model.QueryResponse, error)
	Query(ctx context.Context, request model.QueryRequest) (*model.QueryResponse, error)
	QueryByID(ctx context.Context, namespace string, id string) (*model.QueryResponse, error)

	Fetch(ctx context.Context, request model.FetchRequest) (*model.FetchResponse, error)

	UpdateRequest(ctx context.Context, namespace string, id string, vector []float64, metadata interface{}) (*model.UpdateResponse, error)
	Update(ctx context.Context, request model.UpdateRequest) (*model.UpdateResponse, error)

	DeleteRequest(ctx context.Context, namespace string, ids []string) (*model.DeleteResponse, error)
	Delete(ctx context.Context, request model.DeleteRequest) (*model.DeleteResponse, error)
}

func (c *VectorSearchClient) Fetch(ctx context.Context, request model.FetchRequest) (*model.FetchResponse, error) {
	req, err := c.newRequest(ctx, "GET", "/vectors/fetch", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.FetchResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}

func (c *VectorSearchClient) QueryRequestWithFilter(ctx context.Context, namespace string, knowledge_list []string, top_k int, vector []float64) (*model.QueryResponse, error) {
	filter := map[string]interface{}{
		"knowledge_id": map[string]interface{}{
			"$in": knowledge_list,
		},
	}
	/* pinecone query with filter 失敗範例
		namespace >>>>  ai-amaze-knowledges-dev
	    filter---------- map[knowledge_id:map[$in:['f9f9e398-6f1b-4857-984e-12a033f70217']]]PCGPT 08 err= <nil>
	*/
	/* pinecone query with filter 成功範例
		namespace >>>>  ai-amaze-knowledges-dev
	    filter---------- map[knowledge_id:map[$in:[f9f9e398-6f1b-4857-984e-12a033f70217]]]PCGPT 08 err= <nil>
	*/

	request := model.QueryRequest{
		Vector:          vector,
		Namespace:       namespace,
		Filter:          filter,
		TopK:            top_k,
		IncludeValues:   false,
		IncludeMetadata: true,
	}
	//	fmt.Println("namespace >>>> ", namespace)
	response, err := c.Query(ctx, request)
	//	fmt.Println("Query =======, err=", err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) QueryProductRequestWithFilter(ctx context.Context, namespace string, knowledge_list []string, top_k int, vector []float64, price int, category []string, orderBy string) (*model.QueryResponse, error) {
	filter := make(map[string]interface{})

	if price > 0 {
		filter["price"] = map[string]interface{}{
			"$lt": price,
		}
	}

	filter["knowledge_id"] = map[string]interface{}{
		"$in": knowledge_list,
	}

	if len(category) != 0 {
		filter["category"] = map[string]interface{}{
			"$in": category,
		}
	}

	request := model.QueryRequest{
		Vector:          vector,
		Namespace:       namespace,
		Filter:          filter,
		TopK:            top_k,
		IncludeValues:   false,
		IncludeMetadata: true,
	}
	response, err := c.Query(ctx, request)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) QueryRequest(ctx context.Context, namespace string, top_k int, vector []float64) (*model.QueryResponse, error) {
	request := model.QueryRequest{
		Vector:    vector,
		Namespace: namespace,
		TopK:      top_k,
	}
	response, err := c.Query(ctx, request)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) QueryByID(ctx context.Context, namespace string, id string) (*model.QueryResponse, error) {
	request := model.QueryRequest{
		Id:        id,
		TopK:      1,
		Namespace: namespace,
	}

	response, err := c.Query(ctx, request)
	if err != nil {
		return nil, err
	}
	_, err = json.Marshal(response)
	if err != nil {
		return nil, err
	}

	// jsonString := string(jsonBytes)

	return response, nil
}

func (c *VectorSearchClient) Query(ctx context.Context, request model.QueryRequest) (*model.QueryResponse, error) {
	req, err := c.newRequest(ctx, "POST", "/query", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}
	output := new(model.QueryResponse)
	if err := getResponseObject(resp, output); err != nil {
		fmt.Println("getResponseObject err=", err)
		return nil, err
	}
	return output, nil
}

func (c *VectorSearchClient) UpdateRequest(ctx context.Context, namespace string, id string, vector []float64, metadata interface{}) (*model.UpdateResponse, error) {
	updateRequest := model.UpdateRequest{
		ID:     id,
		Values: vector,
		// SparseValues: vector, // TODO
		SetMetaData: metadata,
		Namespace:   namespace,
	}
	response, err := c.Update(ctx, updateRequest)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) Update(ctx context.Context, request model.UpdateRequest) (*model.UpdateResponse, error) {
	req, err := c.newRequest(ctx, "POST", "/vectors/update", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.UpdateResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}

func (c *VectorSearchClient) SendUpsertRequest(namespace string, id string, vector []float64, metadata interface{}) (*model.UpsertResponse, error) {
	vectors := make([]model.UpsertVectors, 0)
	vectors = append(vectors, model.UpsertVectors{
		ID:       id,
		Values:   vector,
		MetaData: metadata,
	})
	fmt.Println(id, "--------- upsert  vector --------- ", metadata)
	upsertRequest := model.UpsertRequest{
		Vectors:   vectors,
		Namespace: namespace,
	}
	response, err := c.doRequest("POST", "/vectors/upsert", upsertRequest)
	// fmt.Println("do_request =======>, err=", err)
	if err != nil {
		return nil, err
	}
	output := new(model.UpsertResponse)
	if err := getResponseObject(response, output); err != nil {
		fmt.Println("getResponseObject =======>, err=", err)
		return nil, err
	}
	return output, nil
}

func (c *VectorSearchClient) UpsertRequest(ctx context.Context, namespace string, id string, vector []float64, metadata interface{}) (*model.UpsertResponse, error) {
	vectors := make([]model.UpsertVectors, 0)
	vectors = append(vectors, model.UpsertVectors{
		ID:     id,
		Values: vector,
		// SparseValues: vector, // TODO
		MetaData: metadata,
	})

	upsertRequest := model.UpsertRequest{
		Vectors:   vectors,
		Namespace: namespace,
	}
	response, err := c.Upsert(ctx, upsertRequest)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) Upsert(ctx context.Context, request model.UpsertRequest) (*model.UpsertResponse, error) {
	req, err := c.newRequest(ctx, "POST", "/vectors/upsert", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}
	output := new(model.UpsertResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}

func (c *VectorSearchClient) DeleteRequest(ctx context.Context, namespace string, ids []string) (*model.DeleteResponse, error) {
	deleteRequest := model.DeleteRequest{
		IDs:       ids,
		Namespace: namespace,
	}
	response, err := c.Delete(ctx, deleteRequest)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *VectorSearchClient) Delete(ctx context.Context, request model.DeleteRequest) (*model.DeleteResponse, error) {
	req, err := c.newRequest(ctx, "POST", "/vectors/delete", request)
	if err != nil {
		return nil, err
	}
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, err
	}

	output := new(model.DeleteResponse)
	if err := getResponseObject(resp, output); err != nil {
		return nil, err
	}
	return output, nil
}
