package model

type Index struct {
	Name             string      `json:"name"`
	Dimension        int         `json:"dimension"`
	Metric           string      `json:"metric"`
	Pods             int         `json:"pods"`
	Replicas         int         `json:"replicas"`
	PodType          string      `json:"pod_type"`
	MetadataConfig   interface{} `json:"metadata_config"`
	SourceCollection string      `json:"source_collection"`
}

type CreateIndexResponse struct{}

type DescribeIndexResponse struct {
	/*
		Name      string `json:"name"`
		Dimension string `json:"dimension"`
		Metric    string `json:"metric"`
		Pods      int    `json:"pods"`
		Replicas  int    `json:"replicas"`
		Shards    int    `json:"shards"`
		PodType   string `json:"pod_type"`
		//IndexConfig    interface{} `json:"index_config"`
		//MetadataConfig interface{} `json:"metadata_config"`
		//Status         interface{} `json:"status"`
	*/
}

type Database struct {
	/*

	 */
}

// CreateIndexOption are options that can be passed when creating a new index
type IndexOption func(*Index) error

func Dimension(dimension int) IndexOption {
	return func(c *Index) error {
		c.Dimension = dimension
		return nil
	}
}

func Metric(metric string) IndexOption {
	return func(c *Index) error {
		c.Metric = metric
		return nil
	}
}

func Pods(pods int) IndexOption {
	return func(c *Index) error {
		c.Pods = pods
		return nil
	}
}

func Replicas(replicas int) IndexOption {
	return func(c *Index) error {
		c.Replicas = replicas
		return nil
	}
}

func PodType(pod_type string) IndexOption {
	return func(c *Index) error {
		c.PodType = pod_type
		return nil
	}
}

func MetadataConfig(metadata_config interface{}) IndexOption {
	return func(c *Index) error {
		c.MetadataConfig = metadata_config
		return nil
	}
}

func SourceCollection(source_collection string) IndexOption {
	return func(c *Index) error {
		c.SourceCollection = source_collection
		return nil
	}
}
