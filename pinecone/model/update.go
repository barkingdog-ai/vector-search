package model

type UpdateRequest struct {
	ID     string    `json:"id"  binding:"required"`
	Values []float64 `json:"values" binding:"required"` // This is the vector data included in the request.
	// SparseValues SparseVector `json:"sparseValues"`  // Vector sparse data. Represented as a list of indices and a list of corresponded values, which must be the same length.
	SetMetaData interface{} `json:"setMetadata"`
	Namespace   string      `json:"namespace"`
}

type UpdateResponse struct{}
