package model

type UpsertRequest struct {
	Vectors   []UpsertVectors `json:"vectors"`
	Namespace string          `json:"namespace"`
}

// https://docs.pinecone.io/reference/upsert/
type UpsertVectors struct {
	ID     string    `json:"id" binding:"required"`     // This is the vector's unique id.
	Values []float64 `json:"values" binding:"required"` // This is the vector data included in the request.
	// SparseValues SparseVector `json:"sparseValues"`  // Vector sparse data. Represented as a list of indices and a list of corresponded values, which must be the same length.
	MetaData interface{} `json:"metadata"`
}
type UpsertResponse struct{}
