package model

type FetchRequest struct {
	Namespace string   `json:"namespace"`
	Ids       []string `json:"ids"`
}

type Vector struct {
	ID       string      `json:"id"`
	Values   []float64   `json:"values"`
	MetaData interface{} `json:"metadata"`
}

type FetchResponse struct {
	Vectors   map[string]Vector `json:"vectors"`
	Namespace string            `json:"namespace"`
}
