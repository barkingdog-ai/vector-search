package model

type DeleteRequest struct {
	IDs       []string       `json:"ids"`
	DeleteAll bool           `json:"deleteAll"`
	Namespace string         `json:"namespace"`
	Filter    map[string]any `json:"filter,omitempty"`
}

type DeleteResponse struct{}
