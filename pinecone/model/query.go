package model

type MetadataFilter struct {
	Key    string   `json:"key"`
	Values []string `json:"values"`
}

// https://docs.pinecone.io/reference/query
type QueryRequest struct {
	Namespace       string    `json:"namespace"`
	TopK            int       `json:"topK"`            // The number of results to return for each query.
	IncludeValues   bool      `json:"includeValues"`   // Indicates whether vector values are included in the response.
	IncludeMetadata bool      `json:"includeMetadata"` // Indicates whether metadata is included in the response as well as the ids.
	Vector          []float64 `json:"vector"`          // The query vector. This should be the same length as the dimension of the index being queried. Each query() request can contain only one of the parameters id or vector.
	// SparseVector    SparseVector `json:"sparseVector"`    //Vector sparse data. Represented as a list of indices and a list of corresponded values, which must be the same length.
	Id     string                 `json:"id"` // The unique ID of the vector to be used as a query vector. Each query() request can contain only one of the parameters queries, vector, or id.
	Filter map[string]interface{} `json:"filter"`
}

type SparseVector struct {
	Indices []int64   `json:"indices"`
	Values  []float32 `json:"values"`
}

type QueryResponse struct {
	Matches   []QueryMatch `json:"matches"`
	Namespace string       `json:"namespace"`
}

type QueryMatch struct {
	ID    string  `json:"id"`
	Score float64 `json:"score"`
	// Values   []float64 `json:"values"`
	// SparseValues string `json:"sparseValues"`
	MetaData map[string]interface{} `json:"metadata"`
}
