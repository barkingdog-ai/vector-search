package utils

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func ToSnakeCase(str string) string {
	matchFirstCap := regexp.MustCompile("(.)([A-Z][a-z]+)")
	matchAllCap := regexp.MustCompile("([a-z0-9])([A-Z])")

	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}

// 將字串轉換成float32
func ConvertStringToFloat32(str string) (float32, error) {
	var result float64
	result, err := strconv.ParseFloat(str, 32)
	if err != nil {
		return 0, fmt.Errorf("failed to convert string to float32: %w", err)
	}
	return float32(result), nil
}

// 將字串轉換成float64
func ConvertStringToFloat64(str string) (float64, error) {
	var result float64
	result, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to convert string to float64: %w", err)
	}
	return result, nil
}

// 將字串轉換成int
func ConvertStringToInt(str string) (int, error) {
	var result int
	result, err := strconv.Atoi(str)
	if err != nil {
		return 0, fmt.Errorf("failed to convert string to int: %w", err)
	}
	return result, nil
}

func ConvertFloat32ToString(f float32) string {
	return strconv.FormatFloat(float64(f), 'f', -1, 32)
}

func ConvertSnakecaseToCamelCase(snake string) string {
	// Split the string into words.
	words := strings.Split(snake, "_")

	// Capitalize the first letter of each word.
	for i, word := range words {
		words[i] = strings.Title(word)
	}

	// Join the words back into a string.
	return strings.Join(words, "")
}
