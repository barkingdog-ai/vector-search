package utils

import (
	"fmt"
	"os"
)

// 只有在第一次初始化的時候使用此function, 不要再執行過程供使用, 不然會導致app強制關閉
func MustGetEnv(name string) string {
	myVar := os.Getenv(name)
	if myVar == "" {
		msg := fmt.Sprintf("ENVIRONMENT VARIABLE: %s is not set", name)
		panic(msg)
	}
	return myVar
}
