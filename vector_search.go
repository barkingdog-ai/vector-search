package vector_serach

import (
	"net/http"
	"time"

	API "gitlab.com/barkingdog-ai/vector-search/pinecone/api"
)

const (
	defaultBaseURL        = ""
	defaultTimeoutSeconds = 30
)

// A Client is an API client to communicate with the Pinecone APIs
type Interface interface {
	API.IndexInterface
	// API.CollectionInterface
	API.VectorInterface
}

// NewClient returns a new Pinecone API client. An apiKey is required to use the client
func NewClient(apiKey string, indexName string, options ...API.ClientOption) (Interface, error) {
	httpClient := &http.Client{
		Timeout: time.Duration(defaultTimeoutSeconds * time.Second),
	}

	c := &API.VectorSearchClient{
		ApiKey:     apiKey,
		IndexName:  indexName,
		BaseURL:    "https://" + indexName,
		HttpClient: httpClient,
	}
	for _, o := range options {
		o(c)
	}
	return c, nil
}
